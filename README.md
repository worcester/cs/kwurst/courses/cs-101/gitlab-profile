# CS-101 Basics of Computer Science
This is a LASC course and can be taken to satisfy the *Quantitative Reasoning* requirement.

> *A survey course that provides a foundation in the field of Computer Science by presenting a practical and realistic understanding of the field.*

**Karl R. Wurst**
<br>Worcester State University

Upon successful completion of this course, students will be able to:

* Understand and appreciate the notion of algorithms and abstraction.
* Have a firm grasp on the principles of digital representation of data and of basics of using such data in a variety of applications.
* Understand the basics of computer architecture and learn how computers are programmed by means of encoded instructions.
* Understand the fundamentals of operating systems – what they do and how they do it.
* Understand the basic operation of networks, application of networks and security issues.
* Understand the role of general information systems such as spreadsheets and databases.
* Appreciate how algorithms are the underpinnings of the science of computing.  
* Learn how to write programs in pseudo code efficiently and how to read code and understand pseudo code.
* Understand the abstractions behind common computer programming languages.

## LASC Quantitative Reasoning Outcomes
This course will:

1.	Acquaint students with formal systems, procedures and sequences of operation.
2.	Strengthen students' understanding of variables and functions.
3.	Apply mathematical techniques to the analysis and solution of real-life problems.
4.	Emphasize the importance of accuracy, including precise language and careful definitions of mathematical concepts.
5.	Understand both the underlying principles and practical applications of one or more fields of mathematics.
6.	Strengthen understanding of the relationship between algebraic and graphical representations.

##Copyright and License
####&copy; 2014 Karl R. Wurst, Worcester State University

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.