*Version 2020-Fall-1.0, 31 August 2020*

### *CS-101 02/02S &mdash; Fall 2020*

# CS-101 Basics of Computer Science
This is a LASC course and can be taken to satisfy the *Quantitative Reasoning* requirement.

## Credit and Contact Hours
3 credits

## Catalog Course Description
> *A survey course that provides a foundation in the field of Computer Science by presenting a practical and realistic understanding of the field.*

## Instructor
Dr. Karl R. Wurst<br>
See <a href="http://cs.worcester.edu/kwurst/" target="_blank">http://cs.worcester.edu/kwurst/</a> for contact information and schedule.

## Meeting Times and Locations
Meets on Zoom, TR 8:30-9:45am

## It's in the Syllabus
<img src=http://www.phdcomics.com/comics/archive/phd051013s.gif><br>
<a href="http://www.phdcomics.com/comics.php?f=1583">http://www.phdcomics.com/comics.php?f=1583</a>

**If you don't find the answer to your question in the syllabus, then please ask me.**

## Textbook
<img src=http://www.jblearning.com/covers/full/1284055914.jpg align=left width=100 style="padding-right:10px">

*Computer Science Illuminated, Seventh edition*<br>
Nell Dale and John Lewis<br>
Jones & Bartlett Learning, 2020<br>
ISBN-13: 978-1-284-15561-7<br>
<a href="http://www.jblearning.com/catalog/9781284155617/" target="_blank">Textbook website</a>

## Required Materials
In addition to the textbook, to successfully complete this course you will need:

1. **Computer**: You will need a computer that you can use at home. The brand and operating system (Windows, Mac OS X, Linux) is unimportant – the software we will be using runs on all major operating systems and most of it can be downloaded for free or low cost.  The software you will need installed:
	1. **Web Browser** &mdash; You will need a web browser to access Zoom for class sessions, Google Drive, the Blackboard course site, Piazza, and online simulators and tools. (see *Internet Access* below.)
	2. **Word processor** &mdash; Students will be required to use a word processor as the primary tool for many class assignments. Students are expected to quickly develop their own proficiency at creating, storing and editing files of their own design. Such proficiency should be common by the second week of class.
	3. **Other software as needed** &mdash; We may install other software as needed. All software will be free to download and install, and the instructor will help with installation.
2.	**Internet Access**: You will need internet access for access to:
	1. **Zoom** 7mdash; We will be using Zoom for your synchronous class sessions.
	2. **Google Docs** &mdash; We will be using Google Docs as a shared-editing space for teams to work on in-class activities.
	3. **Blackboard** &mdash; All course materials and announcements will be made available through the course site on Blackboard. Quizzes and exams will be given on Blackboard. Students will be required to use Blackboard as the course tool and be familiar with uploading files. 
	4. **WSU Gmail** &mdash; You must check your WSU Gmail account on a regular basis. All communications to the class, such as corrections to problem sets or changes in due dates, will be sent to your WSU Gmail account.
	5. **Piazza**&mdash;This is an online service that lets you ask (and answer other students’) questions about the course assignments, materials, and topics.
	5. **Online simulators and tools** &mdash; We will be using a number of online simulators and tolls to allow students to try out some Computer Science concepts, rather than just reading about them.

## Course Workload Expectations
***This is a three-credit course. You should expect to spend, on average, 9 hours per week on this class.***

You should expect to spend, on average, 9 hours per week during the semester reading the textbook, studying, doing assignments, working on projects, and practicing in order to master the concepts and use of the materials covered in the course. (See Definition of the Credit Hour)

## Definition of the Credit Hour
> Federal regulation defines a credit hour as an amount of work represented in intended learning outcomes and verified by evidence of
student achievement that is an institutional established equivalence that reasonably approximates not less than –

> 1.	One hour of classroom or direct faculty instruction and a minimum of two hours of out of class student work each week for
approximately fifteen weeks for one semester or trimester hour of credit, or ten to twelve weeks for one quarter hour of credit, or
the equivalent amount of work over a different amount of time; or
> 2.	At least an equivalent amount of work as required in paragraph (1) of this definition for other academic activities as
established by the institution including laboratory work, internships, practica, studio work, and other academic work leading to the
award of credit hours.

> &mdash; New England Commission of Higher Education, <a href="https://www.neche.org/wp-content/uploads/2018/12/Pp111_Policy_On_Credits-And-Degrees.pdf" target="_blank">
Policy on Credits and Degrees</a>

## Prerequisites

* Since this course has a prerequisite of **"Familiarity with basic computer operations"** you should be comfortable with email, web browsing and elementary word processing. It is expected that you can use these basic computer applications for completing work in this course without additional help from the instructor.

 ***If you do not have this computer familiarity background, you should not take this course.***

* Since this course has a Mathematics prerequisite of **"Math placement code of 3 or above"** you should have basic facility with arithmetic. In addition, it is expected that you have a basic facility with elementary algebra. We will be working with the concepts of variables and functions.

 ***If you do not have this mathematical background, you should not take this course.***

**No prior programming experience is needed or expected for this course.**

## Course Objectives
The objective of this course is to give you a broad overview of the field of Computer Science.

* If you are not a CS Major or Minor the course should answer the question: *"What should you know about how a computer works, what it can and cannot do, and how it stores and manipulates information?"*
* If you are a CS Major or Minor the course should answer the question: *"What areas will you explore in more detail as you progress through the program?"*

## Course-Level Student Learning Outcomes
Upon successful completion of this course, students will be able to:

* Understand and appreciate the notion of algorithms and abstraction.
* Have a firm grasp on the principles of digital representation of data and of basics of using such data in a variety of applications.
* Understand the basics of computer architecture and learn how computers are programmed by means of encoded instructions.
* Understand the fundamentals of operating systems&mdash;what they do and how they do it.
* Understand the basic operation of networks, applications of networks and security issues.
* Understand the role of general information systems such as spreadsheets and databases.
* Appreciate how algorithms are the underpinnings of the science of computing.  
* Learn how to write programs in pseudocode efficiently and how to read code and understand pseudocode.
* Understand the abstractions behind common computer programming languages.

## LASC Student Learning Outcomes
This course will contribute to the students' ability to:

* Demonstrate effective oral and written communication.
* Employ quantitative and qualitative reasoning.
* Apply skills in critical thinking.
* Apply skills in information literacy.
* Understand the roles of science and technology in our modern world.
* Understand how scholars in various disciplines approach problems and construct knowledge.
* Display socially responsible behavior and act as socially responsible agents in the world.

## LASC Quantitative Reasoning Outcomes
This course will:

1. Acquaint students with formal systems, procedures and sequences of operation.
2. Strengthen students' understanding of variables and functions.
3. Apply mathematical techniques to the analysis and solution of real-life problems.
4. Emphasize the importance of accuracy, including precise language and careful definitions of mathematical concepts.
5. Understand both the underlying principles and practical applications of one or more fields of mathematics.
6. Strengthen understanding of the relationship between algebraic and graphical representations.

## Program-Level Student Learning Outcomes
This course addresses the following outcomes of the Computer Science Major:

Upon successful completion of the Major in Computer Science, students will be able to:

1.	Analyze a problem, develop/design multiple solutions and evaluate and document the solutions based on the requirements. (Introduction)
2.	Communicate effectively both in written and oral form. (Introduction)
3.	Identify professional and ethical considerations, and apply ethical reasoning to technological solutions to problems. (Introduction)
4.	Demonstrate an understanding of and appreciation for the importance of negotiation, effective work habits, leadership, and good communication with teammates and stakeholders. (Introduction)
5.	Learn new models, techniques, and technologies as they emerge and appreciate the necessity of such continuing professional development. (Introduction)


## Course Topics
The course outline will be covered on a best-effort basis, subject as always to time limitations as the course progresses. 
 
* Data Representation (Chapter 2, 3)
* Digital Circuits (Chapter 4, 5)
* Basic Computer Organization and Assembly Language (Chapter 6)
* Algorithms and Efficiency of Algorithms (Chapters 6, 7)
* Programming Language Models (Chapters 8, 9)
* Operating Systems (Chapter 10)
* File Systems (Chapter 11)
* Information Systems (Chapter 12)
* Artificial Intelligence (Chapter 13)
* Simulation, Graphics and Other Applications (Chapter 14)
* Networking (Chapter 15)
* The World Wide Web (Chapter 16)
* Computer Security (Chapter 17)
* Limits of Computing (Chapter 18)

## Course Philosophy
The primary goal of this course is to provide a basic understanding of what computers are and how they work. To do this we must not only understand something about the electronic circuitry of computers, but also how data representing the world around us (text, numbers, images, sounds, etc.) is represented inside the computer, and how the computer can manipulate, store, and transmit this data. Some programming concepts are introduced to motivate the topics, but programming is not the main focus of the course.

We will explore these topics through in-class activities, readings, and assignments. The goal is not to make you an expert, but to provide you a basic understanding and appreciation of how computers do what they do, and to provide you with a basis for further study in the field of Computer Science.

You are encouraged to help each other out, in and out of the classroom, as long as you do your own work. (See *Academic Conduct* below.)

## Instructional Method

This class will not be a traditional “lecture” class, and will incorporate some teaching methods that may be unfamiliar to you.

### POGIL
Rather than listening to lectures about the course content, you and your classmates will "discover" the content for yourselves through small-group work.

The group work will be a very structured style called Process Oriented Guided Inquiry Learning (POGIL). Through investigation of models of the concepts and answering questions that guide the team toward understanding of the models, your team will learn both the content and team process skills. In your POGIL groups each group member will have a specific role to play during the activity, and roles will be rotated so that everyone will get to experience a variety of process skills.

For more information on POGIL, see [https://pogil.org/about-pogil/what-is-pogil](https://pogil.org/about-pogil/what-is-pogil).

## Grading Policies

### Assignment Grading

All assignments in this course will be graded on a 3-0 point scale, with the following general rubric:

3 points | 2 points | 1 point | 0 points
:-: | :-: | :-: | :-:
Essentially perfect | Mostly correct | Mostly incorrect | Not submitted

It will be possible to revise and resubmit a limited number of assignments with a grade of less than 3 points (see *Revision and Resubmission of Work* below).

### Course Grade Determination

#### Base Grade

Assignment | Earn Base Grade<br>A | Earn Base Grade<br>B | Earn Base Grade<br>C | Earn Base Grade<br>D 
--- | :-: | :-: | :-: | :-:
Class Attendance and Participation<br>&nbsp;(out of 27 classes) | 25 | 24 | 23 | 22
Readings Quizzes Average | &ge; 90% | &ge; 80% | &ge; 70%  | &ge; 60%
Homework Assignments<br>(17, graded on a 3-0 point scale, for a maximum of 51 points)  | 42 points | 34 points | 25 points | 17 points
Programming Projects<br>(4, graded on a 3-0 point scale, for a maximum of 12 points) | 9 points | 7 points | 5 points | 4 points
Exam Grade Average (3 exams) | > 50% | > 50% | > 50% | &le; 50%

* **Failing to meet the all the requirements for a particular letter grade will result in not earning that grade.** For example, even if you complete all other requirements for a B grade, but fail to earn 7 points on Programming Projects, you will earn a C grade.
* **Failing to meet the all the requirements for earning a D grade will result in a failing grade for the course.**

#### Plus or Minus Grade Modifiers

* You will have a ***minus*** modifier applied to your base grade if the average of your exam grades is 65% or lower.
* You will have a ***plus*** modifier applied to your base grade if the average of your exam grades is 85% or higher.
* Each unused token remaining at the end of the semester can be used to increase the exam average by 2 percentage points.

*Notes:*
 
* WSU has no A+ grade.
* I reserve the right to revise *downward* the required number of points needed for each base grade due to changes in number of assignments assigned or unexpected difficulties with assignments.

## Attendance and Participation

Because a significant portion of your learning will take place during the in-class activities and as part of your team, class attendance and participation are extremely important.

For your attendance and participation to be considered to *Meet Specification*, you must:

* Arrive on time and stay for the entire class session.
* Participate fully in the in-class activity:
    * Fulfill all responsibilities of your team role.
    * Contribute to the team's learning and answers to questions.
    * Work as part of the team on the activity (not on your own.)
    * Work on the in-class activity (not some other work.)

## Readings and Quizzes
Required reading is assigned from the textbook; additional readings may be linked from the Blackboard site.

You will be taking a quiz on each of the reading assignments that will help me determine if more explanation is needed for certain topics.

You may retake each quiz as often as you wish before the due date.

## Homework Assignments
You will be assigned a reading topic in the chapter (sometimes only certain sections in the chapter) and you will be assigned problems from the book and possibly other problems to solve.  Assignments are to be typed and posted in Blackboard. 

## Programming Projects
Chapters 6-9 are about how computers are programmed and how programs are written. Rather than just reading about programming, we are going to do some simple programming. Most of the programming projects will involve running programs with different inputs to see what they do, reading programs to understand how they work, and making modifications to the programs to make them work differently.

Your programs will not need to work perfectly, but should do most of what they are supposed to do.

## Exams
There will be three exams given during the semester. The exams will be completed outside of class time. The first two exams will be posted after class on a Thursday, and will be due on the following Monday. The third exam will be posted on the Profession Development Day, and will be due by the end of our scheduled final exam period. You may complete the exam in any 2-hour period you wish during that time.

* Exam 1 is tentively scheduled to be posted after class on 8 October 2020, and due on 12 October 2020 by 11:59pm.
* Exam 2 is tentively scheduled to be posted after class on 5 November 2020 and due on 9 November 2020 by 11:59pm.
* Exam 3 will be posted on 10 December 2020 and due on 15 December 2020 by 11:59pm. 
	* Our scheduled final exam period is 15 December 2020, 8:30-11:30am. You do not need to take your exam at that time, but I will be available on Zoom during that time.

## Deliverables
All work must be submitted electronically through the Assignment feature in Blackboard. The due date and time will be given on the assignment.  The submission date and time will be determined by the “Submitted on” timestamp in Blackboard.

**Please do not submit assignments to me via email.** It is difficult for me to keep track of them and I often fail to remember that they are in my mailbox when it comes time to grade the assignment.

It is strongly recommended that you keep copies of your projects. Students are responsible for reproducing any lost work including unreadable files.

Graded assignments (with comments and solutions) will be returned to you electronically. To access a graded assignment, please click on your score for that assignment in the Blackboard Grade Center.  *Please make sure that you review the comments and solutions provided so you can improve your future work.*

### Late Submissions
Late work will not be accepted. (See *Tokens* below.)

## Revision and Resubmission of Work

### For Reading Quizzes

You may retake each quiz as often as you wish before the due date.

### For Homework Assignments and Programming Projects

If you receive less than 3 points on an Assignment you may revise and resubmit the assignment ***one time only*** without the use of a token.

* You must have submitted the original assignment on time, (or one day late with the use of a token.)
* You must submit your revision within one week from the date when the grade and comments were posted in My Grades. (You may use a token to submit the revision one day late.)
* You may ask me for clarification of the assignment, or of the comments I made on your submission.
* You may ask me to look at your revised solution to see if it addresses my comments.
* You must let me know by email when you resubmit the assignment, so that I know to regrade it.

## Tokens
Each student will be able to earn up to 5 tokens over the course of the semester. These tokens will be earned by completing simple set-up and housekeeping tasks for the course.

Each token can be used to:

* replace a single missed class session (up to a maximum of 3 missed class sessions)
* turn in an assignment late by 24 hours
* revise and resubmit an assignment a second time. Any work to be revised and resubmitted must have been submitted by the original due date.
* Each unused token remaining at the end of the semester can be used to increase the exam average by 2 percentage points.


### Token Accounting
* Unused tokens will be kept track of in the Blackboard *My Grades* area.
* Tokens will not be automatically applied. You must explicitly tell me **by email** when you want to use a token, and for which assignment.

## Getting Help
If you are struggling with the material or a project please see me as soon as possible. Often a few minutes of individual attention is all that is needed to get you back on track.

By all means, try to work out the material on your own, but ask for help when you cannot do that in a reasonable amount of time. The longer you wait to ask for help, the harder it will be to catch up. 

**Asking for help or coming to see me during office hours is not bothering or annoying me. I am here to help you understand the material and be successful in the course.**

## Contacting Me
You may contact me by email (<a href="mailto:kwurst@worcester.edu">Karl.Wurst@worcester.edu</a>), telephone (+1-508-929-8728), or see me in my office. My office hours are listed on the schedule on my web page (<a href="http://cs.worcester.edu/kwurst/" target="_blank">http://cs.worcester.edu/kwurst/</a>) or you may make an appointment for a mutually convenient time.

**If you email me, please include "[CS-101]" in the subject line, so that my email program can correctly file your email and ensure that your message does not get buried in my general mailbox.**

**If you email me from an account other than your Worcester State email, please be sure that your name appears somewhere in the email, so that I know who I am communicating with.** 

<img src="http://www.phdcomics.com/comics/archive/phd042215s.gif"><br><a href="http://www.phdcomics.com/comics.php?f=1795">http://www.phdcomics.com/comics.php?f=1795</a>

My goal is to get back to you within 24 hours of your email or phone call (with the exception of weekends and holidays), although you will likely hear from me much sooner. If you have not heard from me after 24 hours, please remind me.

## Code of Conduct/Classroom Civility
All students are expected to adhere to the policies as outlined in the University's Student Code of Conduct.

## Student Responsibilities 

* Contribute to a class atmosphere conducive to learning for everyone by asking/answering questions, participating in class discussions. Don’t just lurk!
* When working with in a team, participate actively. Don't let your teammates do all the work - you won't learn anything that way.
* Seek help when necessary
* Start assignments as soon as they are posted.  Do not wait until the due date to seek help/to do the assignments
* Make use of the student support services (see below)
* Expect to spend at least 9 hours of work per week on classwork.
* Each student is responsible for the contents of the readings, handouts, and homework assignments.

## Accessibility Statement
Worcester State University values the diversity of all of our students, faculty and staff.  We recognize the importance of each student’s contribution to our campus community.  WSU is committed to providing equal access and support to all qualified students through the provision of reasonable accommodations so that each student may fully participate in programs and services at WSU.  If you have a disability that requires reasonable accommodations, please contact Student Accessibility Services at SAS@worcester.edu or 508-929-8733.  Please be aware that accommodations cannot be enacted retroactively, making timeliness a critical aspect for their provision.

## Tutoring Services/Academic Success Center
Tutoring Services are offered through the Academic Success Center (ASC).  The ASC is located on the first floor of the Administration building, A-130.  Tutoring services are provided to students FREE of charge.  Students seeking academic assistance should visit the center as soon as possible or contact the Tutoring Coordinator at 508-929-8139

## The Math Center
The Math Center provides free assistance to students in Mathematics.  It is located on the first floor of the Sullivan Academic Building, S143.

## The Writing Center
The writing center provides free assistance to students in the areas of research and writing.  It is located on the third floor of the Sullivan Academic Building, S306.  To schedule an appointment, please call 508-929-8112 or email the Center at writingcenter@worcester.edu.  To find out more information about the Writing Center including the Center's hours and the Center's Online Writing Lab, visit their website at <a href="http://www2.worcester.edu/WritingCenter/" target="_blank">http://www2.worcester.edu/WritingCenter/</a>.

## Worcester State Library
For research help, go to the Library! The [Worcester State University Library] (http://www.worcester.edu/library), located on the second and third floors of the Learning Resource Center, provides access to print materials and items on course reserve in addition to a wide variety of full-text online resources accessible both on- and off-campus, including e-books, journal articles, newspapers, and magazines. Articles and book chapters not available in full-text through the Library’s resources can be obtained through Interlibrary Loan (ILL). With a little planning, ILL expands your ability to get credible sources about topics you pursue in your coursework. Additionally, as a member library of the [ARC consortium](http://www.worcesterarc.org), WSU students can also access resources at other college libraries.

The librarians at WSU can help you develop research questions, identify research strategies, search for relevant and reliable information and data, select the best sources for your paper or project, and cite that information. A librarian can visit your class, meet with you in person (one-on-one or in groups), and provide assistance via email, phone, or chat. While every WSU Librarian is able to help students with a project in any discipline, each department has a designated librarian to support the research needs of the students in that department. You are welcome to stop by their office, or make an appointment! [http://libguides.worcester.edu/askus](http://libguides.worcester.edu/askus)

## Academic Conduct
Each student is responsible for the contents of the readings, discussions, class materials, textbook and handouts. All work must be done independently unless assigned as a group project. You may discuss assignments and materials with other students, but you should never share answers or files. **Everything that you turn in must be your own original work, unless specified otherwise in the assignment.**

Students may help each other understand the programming language and the development environment but students may not discuss actual solutions, design or implementation, to their programming assignments before they are submitted or share code or help each other debug their programming assignments. The assignments are the primary means used to teach the techniques and principles of computer programming; only by completing the programs individually will students receive the full benefit of the assignments. If you are looking at each other’s code before you submit your own, you are in violation of this policy. 

Students may not use solutions to assignments from any textbooks other than the text assigned for the course, or from any person other than the instructor, or from any Internet site, or from any other source not specifically allowed by the instructor. If a student copies code from an unauthorized source and submits it as a solution to an assignment, the student will receive a 0 for that assignment.

**Any inappropriate sharing of work or use of another's work without attribution will result in a grade of zero on that assignment for all parties involved. If you do so a second time, you will receive an “E” for the course.**

Academic integrity is an essential component of a Worcester State education. Education is both the acquisition of knowledge and the development of skills that lead to further intellectual development. Faculty are expected to follow strict principles of intellectual honesty in their own scholarship; students are held to the same standard. Only by doing their own work can students gain the knowledge, skills, confidence and self-worth that come from earned success; only by learning how to gather information, to integrate it and to communicate it effectively, to identify an idea and follow it to its logical conclusion can they develop the habits of mind characteristic of educated citizens. Taking shortcuts to higher or easier grades results in a Worcester State experience that is intellectually bankrupt.

Academic integrity is important to the integrity of the Worcester State community as a whole. If Worcester State awards degrees to students who have not truly earned them, a reputation for dishonesty and incompetence will follow all of our graduates. Violators cheat their classmates out of deserved rewards and recognition. Academic dishonesty debases the institution and demeans the degree from that institution.  

It is in the interest of students, faculty, and administrators to recognize the importance of academic integrity and to ensure that academic standards at Worcester State remain strong. Only by maintaining high standards of academic honesty can we protect the value of the educational process and the credibility of the institution and its graduates in the larger community.

**You should familiarize yourself with Worcester State College’s Academic Honesty policy. The policy outlines what constitutes academic dishonesty, what sanctions may be imposed and the procedure for appealing a decision. The complete Academic Honesty Policy appears at: [http://www.worcester.edu/Academic-Policies/](http://www.worcester.edu/Academic-Policies/)**

**If you have a serious problem that prevents you from finishing an assignment on time, contact me and we'll come up with a solution.**

## Student Work Retention Policy
It is my policy to securely dispose of student work one calendar year after grades have been submitted for a course.

## Schedule
**This schedule is subject to change.**

&nbsp; | Monday |Tuesday | Thursday
--- | :-- |:-- | :-- 
Week 1 | | 1 September &mdash; **No Class**<br>*Pre-College Conference and Convocation* | 3 September<br>Team Roles and Grading Scheme
Week 2 | | 8 September<br>Chapter 2: Binary Values and Number Systems | 10 September<br>Chapter 2: Binary Values and Number Systems 
Week 3 | | 15 September<br>Chapter 3: Data Representation | 17 September<br>Chapter 3: Data Representation
Week 4 | | 22 September<br>Chapter 4: Gates and Circuits  | 24 September<br>Chapter 4: Gates and Circuits
Week 5 | | 29 September<br>Chapter 5: Computing Components | 1 October<br>Chapter 6:  Low-Level Programing Languages and Pseudocode  
Week 6 | | 6 October<br>Chapter 6:  Low-Level Programing Languages and Pseudocode | 8 October<br>Chapter 7:  Problem Solving and Algorithms<br>**Exam 1 Posted**
Week 7 | 12&nbsp;October<br>**Exam 1 Due** | 13 October<br>Chapter 7: Problem Solving and Algorithms | 15 October<br>Programming in Python
Week 8 | | 20 October<br>Chapter 8: Abstract Data Types and Subprograms | 22 October<br>Chapter 8: Abstract Data Types and Subprograms
Week 9 | | 27 October<br>Chapter 9: Object-Oriented Design and High-Level Programming Languages | 29 October<br>Chapter 9: Object-Oriented Design and High-Level Programming Languages
Week 10 | | 3 November<br>Chapter 10: Operating Systems | 5 November<br>Chapter 10: Operating Systems<br>**Exam 2 Posted**
Week 11 | 9&nbsp;November<br>**Exam 2 Due** | 10 November<br>Chapter 11: File Systems and Directories | 12 November<br>Chapter 12: Information Systems
Week 12 | | 17 November<br>Chapter 13: Artificial Intelligence | 19 November<br>Chapter 14: Simulation, Graphics, Gaming, and other Applications
Week 13 | | 24 November<br>Chapter 15: Networks | 26 November &mdash; **No Class**<br>*Thanksgiving Recess*
Week 14 | | 1 December<br>Chapter 16: The World Wide Web | 3 December<br>Chapter 17: Computer Security
Week 15 | | 8 December<br>Chapter 18: Limitations of Computing | 10 December &mdash; **No Class**<br>*Final Exams*<br>**Exam 3 Posted**
Final<br>Exams | | 15 December<br>***Scheduled Final Exam Period &mdash; 8:30-11:30am***<br>**Exam 3 Due**

&copy;2020 Karl R. Wurst, kwurst@worcester.edu

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit <a href="http://creativecommons.org/licenses/by-sa/4.0/" target="_blank">http://creativecommons.org/licenses/by-sa/4.0/</a> or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.